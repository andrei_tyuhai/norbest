<?php
/**
 * @file
 * Plugin definition for CondeNast home page layout.
 */

$plugin = array(
  'title' => t('Norbest: Certificates'),
  'theme' => 'norbest_certificates',
  'icon' => 'preview.png',
  'category' => 'Norbest',
  'regions' => array(
    'content' => t('Content'),
  ),
);
