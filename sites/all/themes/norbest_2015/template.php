<?php
/**
 * Preprocessor for node.tpl.php template file.
 */
function norbest_2015_preprocess_node(&$vars, $hook) {
  if ($vars['type'] == "recipe" || $vars['type'] == "product") {
    tpl_field_vars_preprocess($vars, $vars['node'], array(
      'cleanup' => TRUE,
      'debug' => FALSE
    ));
  }

  $view_mode = $vars['view_mode'];
  $node = $vars['node'];
  // Add contextual links for header_block view.
  $vars['title_suffix']['contextual_links'] = array(
    '#type' => 'contextual_links',
    '#contextual_links' => array(
      'node' => array(
        'node',
        array($vars['elements']['#node']->nid)
      )
    ),
    '#element' => $vars['elements'],
  );
  // Mark this element as potentially having contextual links attached to it.
  $vars['classes_array'][] = 'contextual-links-region';

  $vars['theme_hook_suggestions'][] = 'node__' . $node->type . '__' . str_replace('-', '_', $view_mode);
  $preprocess = 'norbest_2015_preprocess_node__' . $node->type . '__' . str_replace('-', '_', $view_mode);
  if (function_exists($preprocess)) {
    $preprocess($vars, $hook);
  }

}

function norbest_2015_preprocess_page(&$variables) {
  if (!empty($variables['node']) && !empty($variables['node']->type)) {
    $variables['theme_hook_suggestions'][] = 'page__node__' . $variables['node']->type;
  }
  
  if ($view = views_get_page_view()) {
    $variables['theme_hook_suggestions'][] = 'page__view__' . $view->name;
  }

  //set custom tpl to panel gift-certificates page
  if (arg(0) === 'buy-gift-certificates') {
    $variables['theme_hook_suggestions'][] = 'page__panel__gift_certificates';
  }
}


function norbest_2015_preprocess_entity(&$vars, $hook) {
  if ($vars['elements']['#entity_type'] === 'commerce_order' && $vars['elements']['#view_mode'] === 'checkout_complete') {
    $order = $vars['commerce_order'];
    $order_id = $vars['commerce_order']->order_id;
    $content = $vars['content'];
    // add additional info
    if ($order->data['payment_method'] == 'meshbrains_commerce_check|commerce_payment_meshbrains_commerce_check') {
      $order_data = commerce_embed_view('commerce_cart_summary', 'default', array($order_id));
      $content['commerce_line_items'] = $order_data;
      $content['order_id'] = $order_id;
      $content['mail'] = $order->mail;
    }
    $vars['content'] = $content;
  }
}

/**
 * Preprocess for node--norbest-faq--full.tpl.php
 */
function norbest_2015_preprocess_node__norbest_faq__full(&$vars, $hook) {
  $node = $vars['node'];
  $vars['_html'] = array();
  $questions = array();
  $node_wrapper = entity_metadata_wrapper('node', $node);
  // prepare questions & answers from field_collection.
  if ($node_wrapper->__isset('field_ct_faq_question_category')) {
    $items = $node_wrapper->field_ct_faq_question_category->value();
    if (!empty($items)) {
      foreach ($items as $item) {
        $field_colletion_wrapper = entity_metadata_wrapper('field_collection_item', $item);
        if ($field_colletion_wrapper->__isset('field_fc_category_title')) {
          $category = $field_colletion_wrapper->field_fc_category_title->value();
          if ($field_colletion_wrapper->__isset('field_fc_question_answer')) {
            $items = $field_colletion_wrapper->field_fc_question_answer->value();
            if (!empty($items)) {
              $i = 1;
              foreach ($items as $item) {
                $field_colletion_wrapper = entity_metadata_wrapper('field_collection_item', $item);
                if ($field_colletion_wrapper->__isset('field_faq_question')) {
                  $question = $field_colletion_wrapper->field_faq_question->value();
                  $questions[$category][$i]['question'] = $question;
                }
                if ($field_colletion_wrapper->__isset('field_faq_answer')) {
                  $answer = $field_colletion_wrapper->field_faq_answer->value();
                  $questions[$category][$i]['answer'] = $answer;
                }
                $i++;
              }
            }
          }
        }
      }
      $vars['_html']['questions'] = $questions;
    }
  }
}