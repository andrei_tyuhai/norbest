<?php
/**
 * @file
 * Plugin definition for CondeNast home page layout.
 */

$plugin = array(
  'title' => t('Norbest: Cart Checkout'),
  'theme' => 'conde_checkout',
  'icon' => 'preview.png',
  'category' => 'Norbest',
  'regions' => array(
    'title' => t('Checkout title'),
    'content' => t('Checkout Content'),
  ),
);
