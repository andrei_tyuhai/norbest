<div id="views-bootstrap-accordion-<?php print $id ?>" class="<?php print $classes ?>">
  <?php reset($rows);
        $first = key($rows); ?>
  <?php foreach ($rows as $key => $row): ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a class="accordion-toggle <?php if ($key !== $first): ?>collapsed<?php endif; ?>"
             data-toggle="collapse"
             data-parent="#views-bootstrap-accordion-<?php print $id ?>"
             href="#collapse<?php print $key ?>">
            <?php print $titles[$key] ?>
          </a>
        </h4>
      </div>

      <div id="collapse<?php print $key ?>" class="panel-collapse collapse <?php if($key == '0'): ?>in<?php endif; ?>">
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
              <?php print $row ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endforeach ?>
</div>
