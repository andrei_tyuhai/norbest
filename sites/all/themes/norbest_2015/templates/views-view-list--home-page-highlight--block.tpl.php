<div class='home-page-highlight'>
  <div class='row no-gutters'>
    <?php foreach ($rows as $id => $row): ?>
      <div class='col-xs-12 col-sm-4'>
        <div class='highlight-image'>
          <?php print $row; ?>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>