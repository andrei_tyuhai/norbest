<?php

/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="row">
    <div class="row-height">
      <div class="col-xs-12 col-lg-8 col-height col-top">
        <div class="inside">
        
          <div class='recipe-card'>
            <h3 class='recipe-title'><?php print $title; ?><span class='recipe-yield'>Serves <?php print $servings; ?></span></h3>
            <h4 class='recipe-ingredients'>INGREDIENTS</h4>
            <?php
              $ingredients  = strip_tags($ingredients, '<strong>'); 
              $lines = explode("\n", $ingredients);
              $len = count($lines);
  
              $firsthalf = array_slice($lines, 0, round($len / 2));
              $secondhalf = array_slice($lines, round($len / 2));
  
              print "<div class='row recipe-details'>";
  
              print "<div class='col-xs-12 col-sm-6'>";
              foreach($firsthalf as $line) {
                print "<span itemprop='recipeIngredient'>" . $line . "</span><br>";
              }
              print "</div><!-- .col -->";
  
              print "<div class='col-xs-12 col-sm-6'>";
              foreach($secondhalf as $line) {
                print "<span itemprop='recipeIngredient'>" . $line . "</span><br>";
              }
              print "</div><!-- .col -->";
              print "</div><!-- .row -->";
  
              print "<br>";
            ?>
          </div>
          
          <div class="recipe-support">
            <div class="row">
              <div class="col-xs-4 col-sm-2 col-sm-offset-3">
                <div class="text-center prep-time">
                  Prep Time<br>
                  <strong>
                    <meta itemprop="prepTime" content="PT<?php print $prep_time ?>M">
                      <?php print $prep_time ?> min
                    </meta>
                  </strong>
                </div>
              </div><!-- .col -->
              <div class="col-xs-4 col-sm-2">
                <div class="text-center total-time">
                  Total Time<br>
                  <strong><meta itemprop="totalTime" content="PT<?php print $total_time ?>M">
                    <?php print $total_time ?> min
                  </meta></strong>
                </div>
              </div><!-- .col -->
              <div class="col-xs-4 col-sm-2">
                <div class="text-center servings">
                  Servings<br>
                  <strong>
                    <meta itemprop="recipeYield">
                      <?php print $servings; ?>
                    </meta>
                  </strong>
                </div>
              </div><!-- .col -->
            </div><!-- .row -->
            
            <div class='row'>
              <div class='social-icons'>
                <div class='col-xs-2 col-xs-offset-1 col-sm-1 col-sm-offset-3'>              
                  <a href="<?php url(drupal_get_path_alias('node/' . $node->nid), array('absolute' => TRUE)); ?>" data-image="<?php print $GLOBALS['base_url'] . $image['large_url']; ?>" data-desc="<?php print $title ?>" class="pinterest-color pinit center-block"></a>
                </div>
                <div class='col-xs-2 col-sm-1'>
                  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php print url(drupal_get_path_alias('node/' . $node->nid), array('absolute' => TRUE)); ?>" target="_blank" class="facebook-color center-block"></a>
                </div>
                <div class='col-xs-2 col-sm-1'>
                  <a href="#" class="print-color center-block" onclick="window.print();return false;"></a>
                </div>
                <div class='col-xs-2 col-sm-1'>
                  <a href="mailto:someone@example.com?subject=Excited%20about%20Norbest&amp;body=Come%20visit%20<?php print url(drupal_get_path_alias('node/' . $node->nid), array('absolute' => TRUE)); ?>" target="_top" class='email-color center-block'></a>
                </div>
                <div class='col-xs-2 col-sm-1'>
                  <a href="https://plus.google.com/111015258012716098908?prsrc=3" rel="publisher" target="_top" style="text-decoration:none;" class="center-block">
                    <img src="/sites/all/themes/norbest_2015/images/gplus_color.png" alt="Google+" style="border:0;width:42px;height:42px;"/>
                  </a>
                </div>
              </div><!-- .social-icons -->
            </div><!-- .row -->
          </div><!-- .recipe-support -->
          
          <div class='recipe-body'>
            <span itemprop="description">
              <?php if (!empty($body['value'])): ?>
              <?php print $body['value'] ?>
              <?php endif; ?>
            </span>
          </div>
         
          <h4 class='directions-title'>DIRECTIONS</h4>
          <span itemprop="recipeInstructions" class='directions-details'>
          <?php print $directions; ?>
          </span>
        </div><!-- .inside-->
      </div><!-- .col -->
      
      <div class="hidden-xs hidden-sm hidden-md">
	      <div class="col-xs-12 col-lg-4 col-height col-top">
	        <div class="inside-full-height">
	          <div class="shopping-list">
	            <p class="shopping-list-title">Shopping List</p>
	            <div class="notes">
	              <div></div>
	              <div></div>
	              <div></div>
	              <div></div>
	              <div></div>
	              <div></div>
	              <div></div>
	              <div></div>
	              <div></div>
	              <div></div>
	              <div></div>
	              <div></div>
	              <div></div>
	              <div></div>
	              <div></div>
	              <div></div>
	              <div></div>
	              <div></div>            
	            </div><!-- .notes -->
	          </div><!-- .shopping-list -->
	        </div><!-- .inside -->
	      </div><!-- .col -->
      </div><!-- .hidden-* -->
    </div><!-- .row-height -->
  </div><!-- .row -->
</div>
