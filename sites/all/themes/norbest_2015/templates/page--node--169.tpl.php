<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<div id="wrap">
  <header id="navbar" role="banner" class="<?php print $navbar_classes; ?>">
    <div class="container"> 
      <div class="logo-container">
        <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      </div>
      <?php if (!empty($page['navigation'])): ?>
        <div class="row">
          <div class="col-xs-6 col-xs-offset-6 col-sm-7 col-sm-offset-5 col-md-8 col-md-offset-4">
            <?php print render($page['navigation']); ?>
          </div>
        </div>
      <?php endif; ?>  
    </div><!-- .container -->
  </header>
  
  <div class="fluid-container">
    <header role="banner" id="page-header">
      <?php if (!empty($site_slogan)): ?>
        <p class="lead"><?php print $site_slogan; ?></p>
      <?php endif; ?>
  
      <?php print render($page['header']); ?>
    </header> <!-- /#page-header -->
  </div>
  
  <div id="about-us" class="main-container container">
    <div class="row">
      <div class="col-xs-12 no-gutter">
        <a href="/one-secluded-valley">
          <img src='/sites/all/themes/norbest_2015/images/tiles/slider.jpg' class='img-responsive center-block'>
        </a>
      </div>
    </div><!-- .row --> 

    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-8 no-gutter">
        <a href="https://www.theapplicantmanager.com/careers?co=nb">
          <img src='/sites/all/themes/norbest_2015/images/tiles/careers-tile.jpg' class='img-responsive'>
        </a>
      </div>
      
      <div class="col-xs-12 col-sm-6 col-md-4 no-gutter">
        <a href="/about-us/welcome">
          <img src='/sites/all/themes/norbest_2015/images/tiles/moroni-feed-company-tile.jpg' class='img-responsive'>
        </a>
      </div>

      <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="row">
          <div class="col-xs-6 no-gutter">
            <a href="/about-us/welcome">
              <img src='/sites/all/themes/norbest_2015/images/tiles/raising-turkeys-tile.jpg' class='img-responsive'>
            </a>
          </div><!-- .col -->
          
          <div class="col-xs-6 no-gutter">
            <a href="/about-us/welcome">
              <img src='/sites/all/themes/norbest_2015/images/tiles/agriculture-facts-tile.jpg' class='img-responsive'>
            </a>
          </div><!-- .col -->
        </div><!-- .row -->
        
        <div class="row">
          <div class="col-xs-6 no-gutter">
            <a href="/faqs/purchaser">
              <img src='/sites/all/themes/norbest_2015/images/tiles/faqs-tile.jpg' class='img-responsive'>
            </a>
          </div><!-- .col -->
          <div class="col-xs-6 no-gutter">
            <a href="/about-us/welcome">
              <img src='/sites/all/themes/norbest_2015/images/tiles/job-opportunities-tile.jpg' class='img-responsive'>
            </a>
          </div><!-- .col -->
        </div><!-- .row -->
      </div><!-- .col -->
      
      <div class="col-xs-12 col-md-8">
        <div class="row">
          <div class="col-xs-12 no-gutter">
            <a href="/about-us/welcome#collapse2">
              <img src='/sites/all/themes/norbest_2015/images/tiles/historical-timeline-tile.jpg' class='img-responsive'>
            </a>
          </div>
        </div>
      </div><!-- .col -->
    </div><!-- .row -->
  </div>
    
    <div class="row">
      <?php if (!empty($page['sidebar_first'])): ?>
        <aside class="col-sm-3" role="complementary">
          <?php print render($page['sidebar_first']); ?>
        </aside>  <!-- /#sidebar-first -->
      <?php endif; ?>
  
      <section<?php print $content_column_class; ?>>
        <div class='row'>
          <div class='col-xs-8 col-xs-offset-4'>
          <?php if (!empty($page['offset_header'])): ?>
            <div><?php print render($page['offset_header']); ?></div>
          <?php endif; ?>
          </div><!-- .col -->
        </div><!-- .row -->
        <a id="main-content"></a>
        <?php print $messages; ?>
        <?php if (!empty($tabs)): ?>
          <?php print render($tabs); ?>
        <?php endif; ?>
        <?php if (!empty($page['help'])): ?>
          <?php print render($page['help']); ?>
        <?php endif; ?>
        <?php if (!empty($action_links)): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
        <?php print render($page['content']); ?>
      </section>
  
      <?php if (!empty($page['sidebar_second'])): ?>
        <aside class="col-sm-3" role="complementary">
          <?php print render($page['sidebar_second']); ?>
        </aside>  <!-- /#sidebar-second -->
      <?php endif; ?>
    </div>
  </div><!-- #main -->
  <div id='bottom' class='container'>
    <div class='row hidden-xs hidden-sm'>
      <div class='col-xs-12'>
        <div class='row no-gutters footer-cols'>
          <?php print render($page['bottom_first']) ?>
        </div><!-- .row -->
      </div><!-- .col -->
    </div><!-- .row -->
    <div class="row">
      <?php print render($page['bottom_second']) ?>
    </div>
  </div><!-- #bottom -->
</div><!-- #wrap -->

<footer class="footer fluid-container">
  <?php print render($page['footer']); ?>
</footer>
