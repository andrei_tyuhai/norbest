<?php
/**
 * @file
 * norbest_global.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function norbest_global_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'articles';
  $view->description = 'List of articles';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Articles';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Articles';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Author Profile Image */
  $handler->display->display_options['fields']['field_author_profile_image']['id'] = 'field_author_profile_image';
  $handler->display->display_options['fields']['field_author_profile_image']['table'] = 'field_data_field_author_profile_image';
  $handler->display->display_options['fields']['field_author_profile_image']['field'] = 'field_author_profile_image';
  $handler->display->display_options['fields']['field_author_profile_image']['label'] = '';
  $handler->display->display_options['fields']['field_author_profile_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_author_profile_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_author_profile_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_author_profile_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Author */
  $handler->display->display_options['fields']['field_author']['id'] = 'field_author';
  $handler->display->display_options['fields']['field_author']['table'] = 'field_data_field_author';
  $handler->display->display_options['fields']['field_author']['field'] = 'field_author';
  $handler->display->display_options['fields']['field_author']['label'] = '';
  $handler->display->display_options['fields']['field_author']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_author']['element_type'] = '0';
  $handler->display->display_options['fields']['field_author']['element_label_colon'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_image']['element_class'] = 'img-responsive';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'article_image',
    'image_link' => 'content',
  );
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image_1']['id'] = 'field_image_1';
  $handler->display->display_options['fields']['field_image_1']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image_1']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image_1']['label'] = '';
  $handler->display->display_options['fields']['field_image_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_image_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image_1']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image_1']['type'] = 'image_url';
  $handler->display->display_options['fields']['field_image_1']['settings'] = array(
    'url_type' => '0',
    'image_style' => 'article_image',
    'image_link' => '',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['text'] = '  <div class=\'article-summary\'>
    <div class=\'row\'>
      <div class=\'col-xs-12\'>
        <div class=\'article-title\'><h2>[title]</h2></div>
      </div>
    </div>

    <div class=\'row\'>
      <div class=\'col-xs-12 col-sm-8\'>
        <div class=\'article-author\'>
          [field_author_profile_image]
          [field_author]
        </div>
      </div>
    </div>

    <div class=\'row\'>
      <div class=\'col-xs-12\'>
        <div class=\'article-thumbnail-image\'>[field_image]</div>
        <div class=\'article-body\'><a href="[path]">[body]</a></div>
      </div>
    </div>

    <div class=\'row\'>
      <div class=\'col-xs-12 article-summary-social-icons\'>
        <a href="http://sandbox.norbest.com[path]" data-desc="[body-summary]" data-image="[field_image_1]" class="pinterest-color pinit"></a>
        <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fsandbox.norbest.com[path]" target="_blank" class="facebook-color"></a>
        <a href="https://twitter.com/norbest_turkey" class=\'twitter-color\' rel=\'nofollow\'></a>
        <a href="#" class="print-color" onclick="window.print();return false;"></a>
        <a href="mailto:someone@example.com?subject=Excited%20about%20Norbest&amp;body=Come%20visit%20[path]" target="_top" class=\'email-color\'></a>
      </div>
    </div> 
 </div>

  <div class="clearfix"></div>
';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '600',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'article' => 'article',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'cooking-journal';

  /* Display: By Taxonomy Term */
  $handler = $view->new_display('page', 'By Taxonomy Term', 'page_1');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Author Profile Image */
  $handler->display->display_options['fields']['field_author_profile_image']['id'] = 'field_author_profile_image';
  $handler->display->display_options['fields']['field_author_profile_image']['table'] = 'field_data_field_author_profile_image';
  $handler->display->display_options['fields']['field_author_profile_image']['field'] = 'field_author_profile_image';
  $handler->display->display_options['fields']['field_author_profile_image']['label'] = '';
  $handler->display->display_options['fields']['field_author_profile_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_author_profile_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_author_profile_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_author_profile_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Author */
  $handler->display->display_options['fields']['field_author']['id'] = 'field_author';
  $handler->display->display_options['fields']['field_author']['table'] = 'field_data_field_author';
  $handler->display->display_options['fields']['field_author']['field'] = 'field_author';
  $handler->display->display_options['fields']['field_author']['label'] = '';
  $handler->display->display_options['fields']['field_author']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_author']['element_type'] = '0';
  $handler->display->display_options['fields']['field_author']['element_label_colon'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_image']['element_class'] = 'img-responsive';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'article_image',
    'image_link' => 'content',
  );
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image_1']['id'] = 'field_image_1';
  $handler->display->display_options['fields']['field_image_1']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image_1']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image_1']['label'] = '';
  $handler->display->display_options['fields']['field_image_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_image_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image_1']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image_1']['type'] = 'image_url';
  $handler->display->display_options['fields']['field_image_1']['settings'] = array(
    'url_type' => '0',
    'image_style' => 'article_image',
    'image_link' => '',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['text'] = '  <div class=\'article-summary\'>
    <div class=\'row\'>
      <div class=\'col-xs-12\'>
        <div class=\'article-title\'><h2>[title]</h2></div>
      </div>
    </div>

    <div class=\'row\'>
      <div class=\'col-xs-12 col-sm-8\'>
        <div class=\'article-author\'>
          [field_author_profile_image]
          [field_author]
        </div>
      </div>
    </div>

    <div class=\'row\'>
      <div class=\'col-xs-12\'>
        <div class=\'article-thumbnail-image\'>[field_image]</div>
        <div class=\'article-body\'><a href="[path]">[body]</a></div>
      </div>
    </div>

 </div>

  <div class="clearfix"></div>
';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Content: Social Sharing */
  $handler->display->display_options['fields']['field_social_sharing']['id'] = 'field_social_sharing';
  $handler->display->display_options['fields']['field_social_sharing']['table'] = 'field_data_field_social_sharing';
  $handler->display->display_options['fields']['field_social_sharing']['field'] = 'field_social_sharing';
  $handler->display->display_options['fields']['field_social_sharing']['label'] = '';
  $handler->display->display_options['fields']['field_social_sharing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_social_sharing']['type'] = 'addthis_basic_toolbox';
  $handler->display->display_options['fields']['field_social_sharing']['settings'] = array(
    'share_services' => 'pinterest_share, facebook, twitter, print, email',
    'buttons_size' => 'addthis_32x32_style',
    'counter_orientation' => 'horizontal',
    'extra_css' => '',
  );
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Has taxonomy term ID */
  $handler->display->display_options['arguments']['tid']['id'] = 'tid';
  $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
  $handler->display->display_options['arguments']['tid']['field'] = 'tid';
  $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'taxonomy_tid';
  $handler->display->display_options['arguments']['tid']['default_argument_options']['term_page'] = FALSE;
  $handler->display->display_options['arguments']['tid']['default_argument_options']['node'] = TRUE;
  $handler->display->display_options['arguments']['tid']['default_argument_options']['vocabularies'] = array(
    'article_categories' => 'article_categories',
  );
  $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
  $handler->display->display_options['arguments']['tid']['validate_options']['vocabularies'] = array(
    'article_categories' => 'article_categories',
  );
  $handler->display->display_options['arguments']['tid']['validate_options']['transform'] = TRUE;
  $handler->display->display_options['path'] = 'taxonomy/term/%';
  $translatables['articles'] = array(
    t('Master'),
    t('Articles'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('  <div class=\'article-summary\'>
    <div class=\'row\'>
      <div class=\'col-xs-12\'>
        <div class=\'article-title\'><h2>[title]</h2></div>
      </div>
    </div>

    <div class=\'row\'>
      <div class=\'col-xs-12 col-sm-8\'>
        <div class=\'article-author\'>
          [field_author_profile_image]
          [field_author]
        </div>
      </div>
    </div>

    <div class=\'row\'>
      <div class=\'col-xs-12\'>
        <div class=\'article-thumbnail-image\'>[field_image]</div>
        <div class=\'article-body\'><a href="[path]">[body]</a></div>
      </div>
    </div>

    <div class=\'row\'>
      <div class=\'col-xs-12 article-summary-social-icons\'>
        <a href="http://sandbox.norbest.com[path]" data-desc="[body-summary]" data-image="[field_image_1]" class="pinterest-color pinit"></a>
        <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fsandbox.norbest.com[path]" target="_blank" class="facebook-color"></a>
        <a href="https://twitter.com/norbest_turkey" class=\'twitter-color\' rel=\'nofollow\'></a>
        <a href="#" class="print-color" onclick="window.print();return false;"></a>
        <a href="mailto:someone@example.com?subject=Excited%20about%20Norbest&amp;body=Come%20visit%20[path]" target="_top" class=\'email-color\'></a>
      </div>
    </div> 
 </div>

  <div class="clearfix"></div>
'),
    t('Page'),
    t('By Taxonomy Term'),
    t('  <div class=\'article-summary\'>
    <div class=\'row\'>
      <div class=\'col-xs-12\'>
        <div class=\'article-title\'><h2>[title]</h2></div>
      </div>
    </div>

    <div class=\'row\'>
      <div class=\'col-xs-12 col-sm-8\'>
        <div class=\'article-author\'>
          [field_author_profile_image]
          [field_author]
        </div>
      </div>
    </div>

    <div class=\'row\'>
      <div class=\'col-xs-12\'>
        <div class=\'article-thumbnail-image\'>[field_image]</div>
        <div class=\'article-body\'><a href="[path]">[body]</a></div>
      </div>
    </div>

 </div>

  <div class="clearfix"></div>
'),
    t('All'),
  );
  $export['articles'] = $view;

  return $export;
}
