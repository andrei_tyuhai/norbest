<?php
/**
 * @file
 *
 * Template for the CondeNast Home layout.
 */
?>

<?php print $content['title']; ?>
<div class="checkout-holder">
  <div class="checkout-content">
    <div class="checkout-block">
      <?php print $content['content']; ?>
    </div>
  </div>
</div>
