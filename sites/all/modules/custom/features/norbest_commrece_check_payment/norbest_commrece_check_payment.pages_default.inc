<?php
/**
 * @file
 * norbest_commrece_check_payment.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function norbest_commrece_check_payment_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'commerce_checkout';
  $page->task = 'page';
  $page->admin_title = 'Commerce Checkout';
  $page->admin_description = '';
  $page->path = 'checkout/%commerce_order/complete';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array(
    'commerce_order' => array(
      'id' => 1,
      'identifier' => 'Commerce Order: ID',
      'name' => 'entity_id:commerce_order',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_commerce_checkout__panel';
  $handler->task = 'page';
  $handler->subtask = 'commerce_checkout';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'access' => array(
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'conde_checkout';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'title' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Thank You For Your Order';
  $display->uuid = '90eaacba-be87-42a6-abee-0e673e23d750';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-823fe06f-957a-4dae-bee7-95bb2eb379ce';
    $pane->panel = 'content';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Print Header',
      'title' => '',
      'body' => '<div class="order-header">
<div class="logo print-only"><img alt="Home" src="/sites/default/files/logo.png" /></div>

<div class="info-text">
<h3>Order Number&nbsp;%commerce_order:order-id</h3>

<div class="red-text">Please mail this document &amp; your check to:</div>
Norbest, LLC<br />
P.O. Box 890<br />
Moroni, UT 84646<br />
Phone: 800-453-5327<br />
Make check payable to: Norbest, LLC.</div>
</div>',
      'format' => 'full_html',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'print-only',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '823fe06f-957a-4dae-bee7-95bb2eb379ce';
    $display->content['new-823fe06f-957a-4dae-bee7-95bb2eb379ce'] = $pane;
    $display->panels['content'][0] = 'new-823fe06f-957a-4dae-bee7-95bb2eb379ce';
    $pane = new stdClass();
    $pane->pid = 'new-4abd8b43-17cc-4524-b7d0-53766356c0c1';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'commerce_order';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'checkout_complete',
      'context' => 'argument_entity_id:commerce_order_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '4abd8b43-17cc-4524-b7d0-53766356c0c1';
    $display->content['new-4abd8b43-17cc-4524-b7d0-53766356c0c1'] = $pane;
    $display->panels['content'][1] = 'new-4abd8b43-17cc-4524-b7d0-53766356c0c1';
    $pane = new stdClass();
    $pane->pid = 'new-d6860535-7416-4eab-a42d-cbb0c1724b76';
    $pane->panel = 'title';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'payment_type',
          'settings' => NULL,
          'context' => 'argument_entity_id:commerce_order_1',
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'admin_title' => 'Thank You For Your Order',
      'title' => '',
      'body' => '<div class="row">
<div class="col-xs-12 col-sm-5 col-sm-offset-4">
<div>
<div class="region region-offset-header">
<div class="no-print">
<h2><span style="font-family:arial,helvetica,sans-serif">Thank You For Your Order</span></h2>

<p>Your order will be processed upon receipt of payment.</p>
</div>
</div>
</div>
</div>
<!-- .col --></div>
',
      'format' => 'full_html',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'no-print',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd6860535-7416-4eab-a42d-cbb0c1724b76';
    $display->content['new-d6860535-7416-4eab-a42d-cbb0c1724b76'] = $pane;
    $display->panels['title'][0] = 'new-d6860535-7416-4eab-a42d-cbb0c1724b76';
    $pane = new stdClass();
    $pane->pid = 'new-20b97a97-024e-48c5-b8aa-4e4d83fc434c';
    $pane->panel = 'title';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'payment_type',
          'settings' => NULL,
          'context' => 'argument_entity_id:commerce_order_1',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'admin_title' => 'Print this Form',
      'title' => '',
      'body' => '<div class="row no-print">
<div class="col-xs-12 col-sm-5 col-sm-offset-4">
<div class="region region-offset-header">
<h2>Print This Form</h2>

<p>Print this order form and mail it along with your check to the address listed below and your order will be processed as soon as it is received.</p>
<a class="print-btn red-text" href="javascript:;" onclick="window.print()" rel="nofollow">PRINT</a></div>
</div>
</div>
',
      'format' => 'full_html',
      'substitute' => 0,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'no-print',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '20b97a97-024e-48c5-b8aa-4e4d83fc434c';
    $display->content['new-20b97a97-024e-48c5-b8aa-4e4d83fc434c'] = $pane;
    $display->panels['title'][1] = 'new-20b97a97-024e-48c5-b8aa-4e4d83fc434c';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-4abd8b43-17cc-4524-b7d0-53766356c0c1';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['commerce_checkout'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'gift_certificates';
  $page->task = 'page';
  $page->admin_title = 'Gift Certificates';
  $page->admin_description = '';
  $page->path = 'buy-gift-certificates';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_gift_certificates__panel';
  $handler->task = 'page';
  $handler->subtask = 'gift_certificates';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'buy-gift-certificates',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'bootstrap_twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'top' => NULL,
      'left' => array(
        'column_type' => 'col-sm',
        'column_size' => '7',
        'offset_type' => 'col-sm-offset',
        'offset_size' => '0',
      ),
      'right' => array(
        'column_type' => 'col-sm',
        'column_size' => '5',
        'offset_type' => 'col-sm-offset',
        'offset_size' => '0',
      ),
      'bottom' => NULL,
    ),
    'left' => array(
      'style' => 'bootstrap_region',
    ),
    'right' => array(
      'style' => 'bootstrap_region',
    ),
  );
  $display->cache = array();
  $display->title = 'Norbest Gift Certificates';
  $display->uuid = '872a39e3-cf3d-4abd-98e6-8d6c7c62d188';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-5523da73-dafa-48e6-aeab-9c68e991cbfb';
    $pane->panel = 'left';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '<p><mediawrapper data=""><img alt="" class="media-element file-default" data-fid="375" data-media-element="1" height="488" src="http://norbest.tyuhai.com/sites/default/files/gift-certificate-teaser_0.jpg" typeof="foaf:Image" width="746" /></mediawrapper></p>
',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5523da73-dafa-48e6-aeab-9c68e991cbfb';
    $display->content['new-5523da73-dafa-48e6-aeab-9c68e991cbfb'] = $pane;
    $display->panels['left'][0] = 'new-5523da73-dafa-48e6-aeab-9c68e991cbfb';
    $pane = new stdClass();
    $pane->pid = 'new-7781522e-19c0-4ae3-9ae6-811fcdde0f01';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Norbest Turkey Gift Certificates',
      'title' => '',
      'body' => '<div class="content cert-content">
<div class="gift-cert-text">
<div class="text-center">
<h2>Norbest Turkey Gift Certificates<br />
<span style="font-family:arial,helvetica,sans-serif"><span style="font-size:20px">A Thoughtful Gift For Any Occasion</span></span><br />
<br />
<span style="font-size:18px"><span style="font-family:arial,helvetica,sans-serif">Meaningful, convenient, and affordable. Turkey gift certificates are ideal for employee appreciation or for showing someone you care.</span></span><br />
&nbsp;</h2>

<p><span style="font-size:16px"><span style="font-family:arial,helvetica,sans-serif">  <em><a href="/faq">This button will link to our FAQ page</a></em></span></span></p>
</div>

<p class="rtecenter">&nbsp;</p>

<p class="rtecenter"><span style="font-family:arial,helvetica,sans-serif">(Click on one of the links below to get started.)</span></p>

<div class="buttons">
<p class="rtecenter"><a href="/gift-certificates/by-weight"><img src="/sites/all/themes/norbest_2015/images/cert_button_by_weight.png" /></a></p>

<h1 class="rtecenter"><br />
<a href="/gift-certificates/by-amount"><img src="/sites/all/themes/norbest_2015/images/cert_button_by_amounght.png" /></a></h1>
</div>
</div>
</div>
',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7781522e-19c0-4ae3-9ae6-811fcdde0f01';
    $display->content['new-7781522e-19c0-4ae3-9ae6-811fcdde0f01'] = $pane;
    $display->panels['right'][0] = 'new-7781522e-19c0-4ae3-9ae6-811fcdde0f01';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['gift_certificates'] = $page;

  return $pages;

}
