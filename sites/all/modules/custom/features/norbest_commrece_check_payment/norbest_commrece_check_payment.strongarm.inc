<?php
/**
 * @file
 * norbest_commrece_check_payment.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function norbest_commrece_check_payment_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'meshbrains_commerce_check_title';
  $strongarm->value = 'Pay By Check';
  $export['meshbrains_commerce_check_title'] = $strongarm;

  return $export;
}
