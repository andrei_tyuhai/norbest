<?php

/**
 * @file
 * Plugin to provide access control/visibility based on order owner
 * commerce_order argument (in URL).
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Norbest: Order payment is check"),
  'description' => t('Access allowed only for orders by check'),
  'callback' => 'order_payment_check_access_check',
  'summary' => 'order_payment_check_access_summary',
  'required context' => new ctools_context_required(t('Order'), 'commerce_order'),
);

/**
 * Check for access.
 */
function order_payment_check_access_check($conf, $context) {

  if (!isset( $context->data)) {
    return FALSE;
  }
  $order = $context->data;
  if ($order->data['payment_method'] === 'meshbrains_commerce_check|commerce_payment_meshbrains_commerce_check') {
    return TRUE;
  }
  else {
    return FALSE;
  }

}

/**
 * Provide a summary description based upon the checked roles.
 */
function order_payment_check_access_summary($conf, $context) {
  return t('Current Order is payment by Check');
}
