<?php
/**
 * @file
 * Bootstrap 8-4 template for Display Suite.
 */
?>


<<?php print $layout_wrapper; print $layout_attributes; ?> class="<?php print $classes; ?>">
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>
  <div class="row">
    <<?php print $left_wrapper; ?> class="col-xs-12 col-lg-8 <?php print $left_classes; ?>">
      <div id="product-image" class="hidden-xs">
        <p id="product-title"></p>
        <p id="product-recipient"></p>
        <p id="product-compliments-of"></p>
        <p id="product-expires"></p>
      </div>
      <?php print $left; ?>
      <p class='product-min-qty'>For orders under 100 certificates a shipping and handling fee of $.50 per certificate will apply at checkout.</p>
    </<?php print $left_wrapper; ?>>
    <<?php print $right_wrapper; ?> class="col-xs-12 col-lg-4 <?php print $right_classes; ?>">
      <?php print $right; ?>
    </<?php print $right_wrapper; ?>>
  </div>
</<?php print $layout_wrapper ?>>


<!-- Needed to activate display suite support on forms -->
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
