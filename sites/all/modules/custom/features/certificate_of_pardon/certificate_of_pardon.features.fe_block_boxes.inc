<?php
/**
 * @file
 * certificate_of_pardon.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function certificate_of_pardon_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Certificate of Pardon Description';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'pardon_certificate_desc';
  $fe_block_boxes->body = '<p><img src="/sites/all/themes/norbest_2015/images/certificate-of-pardon.png" /></p>

<h1 class="rtecenter">Name The Lucky Turkey Sweepstakes</h1>

<div class="col-xs-12 col-sm-8 col-sm-offset-2 text">
<p class="rtecenter"><span style="font-size:20px">One lucky Turkey will be Pardoned by Govener Herbert to live the rest of its days in comfort at Thanksgivng Point. Fill out the form below, enter a name for this lucky bird, and you can win a Grand Prize and a Certificate of Pardon signed by the Governor, if the name you enter is choosen for this luckey bird.</span></p>
</div>';

  $export['pardon_certificate_desc'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Certificate of Pardon Link to Rules';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'pardon_certificate_rules';
  $fe_block_boxes->body = '<p class="rtecenter"><a href="/page/win-right-name-lucky-norbest-turkey-sweepstakes-official-rules" style="color:#000000;text-decoration:underline;">Win the right to name the Lucky Norbest Turkey Sweepstakes Official Rules</a></p>
';

  $export['pardon_certificate_rules'] = $fe_block_boxes;

  return $export;
}
