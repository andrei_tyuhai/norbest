<?php
/**
 * @file
 * norbest_commrece_check_payment.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function norbest_commrece_check_payment_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|gift_certificate_by_amount|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'gift_certificate_by_amount';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|gift_certificate_by_amount|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|gift_certificate_by_weight|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'gift_certificate_by_weight';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|gift_certificate_by_weight|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function norbest_commrece_check_payment_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|gift_certificate_by_amount|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'gift_certificate_by_amount';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'bootstrap_8_4';
  $ds_layout->settings = array(
    'regions' => array(
      'right' => array(
        0 => 'title',
        1 => 'body',
        2 => 'product:field_expires',
        3 => 'field_amount',
      ),
    ),
    'fields' => array(
      'title' => 'right',
      'body' => 'right',
      'product:field_expires' => 'right',
      'field_amount' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|gift_certificate_by_amount|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|gift_certificate_by_weight|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'gift_certificate_by_weight';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'bootstrap_8_4';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'product:field_price_table',
      ),
      'right' => array(
        1 => 'title',
        2 => 'body',
        3 => 'product:field_expires',
        4 => 'field_size',
      ),
    ),
    'fields' => array(
      'product:field_price_table' => 'left',
      'title' => 'right',
      'body' => 'right',
      'product:field_expires' => 'right',
      'field_size' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|gift_certificate_by_weight|default'] = $ds_layout;

  return $export;
}
