<?php
/**
 * @file
 * Provide views data and handlers.
 */

/**
 * Implements hook_views_plugins().
 */
function meshbrains_views_accordion_views_plugins() {
  $module_path = drupal_get_path('module', 'meshbrains_views_accordion');

  return array(
    'style' => array(
      'meshbrains_views_accordion_accordion_plugin_style' => array(
        'title' => t('Meshbrains Accordion'),
        'help' => t('Meshbrains Accordion Style'),
        'path' => $module_path . '/plugins/accordion',
        'handler' => 'MeshbrainsViewsAccordionAccordionPluginStyle',
        'parent' => 'default',
        'theme' => 'meshbrains_views_accordion_accordion_plugin_style',
        'theme path' => $module_path . '/templates/accordion',
        'theme file' => 'theme.inc',
        'uses row plugin' => TRUE,
        'uses grouping' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
