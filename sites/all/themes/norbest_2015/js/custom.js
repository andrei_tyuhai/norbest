(function ($) {
	$(document).ready(function(){
	  //if entering the page with a specific accordion, collapse all and expand the specific accordion
	  if (location.hash){
  	  $('.accordion-toggle').addClass('collapsed');
  	  $('.panel-collapse').removeClass('in');
      $(location.hash).parent().find('.accordion-toggle').removeClass('collapsed');
      $(location.hash).collapse('show');
    }
	
    $(".view-commerce-cart-summary .view-content td,.view-commerce-cart-summary .view-content th").each(function() {
      var $this = $(this);
      $this.html($.trim($this.text()));
    });
    
	  $("p,h1,h2,h3,h4").each(function(){
	    $(this).html($(this).html().replace(/&reg;/gi, '<sup>&reg;</sup>').replace(/®/gi, '<sup>&reg;</sup>'));
	  });

		$(".fb-like-color").fancylike();
		
		$(".pinit").click(function() {
      var url = $(this).attr("href");
      var media = $(this).attr("data-image");
      var desc = $(this).attr("data-desc");
      window.open("//www.pinterest.com/pin/create/button/"+
                  "?url="+url+
                  "&media="+media+
                  "&description="+desc,"_blank");
      return false;
    }); 
    
    $("#edit-line-item-fields-field-recipient-und").change(function(){
      if ( $(this).val() == "Personalize (different name for each certificate)" ) { 
        $("#edit-quantity").val( "0" );
      }
    });     
    
    var expires = $(".field-name-field-expires > .field-items span").text();
    $("#product-expires").html(expires);
    
    $("[id^=edit-attributes-field-turkey-weight--2]").change(function() {
      $("#product-title").html("Whole Turkey " + $(this).val());
      $("#product-title").addClass("highlight").delay(2000).queue(function(){
        $(this).removeClass("highlight").dequeue();
      });
    });
    
    $(".form-item-line-item-fields-field-recipient-und > select").change(function() {
      switch($(this).val()) {
      case "Leave it blank":
        $("#product-recipient").html("");
        break;
      case "Bearer":
        $("#product-recipient").html("Bearer");
        break;
      default:
        $("#product-recipient").html("");
      }
      $("#product-recipient").addClass("highlight").delay(2000).queue(function(){
        $(this).removeClass("highlight").dequeue();
      });
    });    

    $("#edit-line-item-fields-field-custom-recipient-und-0-value").change(function() {
      $("#product-recipient").html($(this).val());
      
      $("#product-recipient").addClass("highlight").delay(2000).queue(function(){
        $(this).removeClass("highlight").dequeue();
      });
    }); 

    $(".field-name-field-compliments-of input").change(function() {  
      $("#product-compliments-of").html($(this).val());

      $("#product-compliments-of").addClass("highlight").delay(2000).queue(function(){
        $(this).removeClass("highlight").dequeue();
      });
    });
	});

  $(function() {
//    $('#want-leftovers').switchButton({
//      on_label: 'Yes',
//      off_label: 'No',
//      width: 40,
//      height: 25,
//      button_width: 20
//    });
    
    $('.commerce-add-to-cart .form-control').attr('autocomplete', 'off');
  });


  $(function() {
    $('.question-list').once('accordion', function () {
      jQuery(this).slideAccordion({
        opener:'>.question',
        slider:'>.answer',
        collapsible:true,
        animSpeed: 300
      });
    });
  });

}(jQuery));


