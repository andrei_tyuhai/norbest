<?php

/**
 * @file
 * Plugin to provide access control/visibility based on order owner
 * commerce_order argument (in URL).
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Norbest: User is owner of the order"),
  'description' => t('Access allowed only for user that is owner of the order'),
  'callback' => 'order_owner_access_check',
  'summary' => 'order_owner_access_summary',
  'required context' => new ctools_context_required(t('Order'), 'commerce_order'),
);

/**
 * Check for access.
 */
function order_owner_access_check($conf, $context) {
  global $user;

  // There should always be a context at this point, but this
  // is safe.
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  // Check access for anonymous.
  if (empty($user->uid)) {
    $current_order = $context->data;

    // Access to cart order.
    if (commerce_cart_order_session_exists($current_order->order_id)) {
      return TRUE;
    }
    // Access to completed order.
    if (commerce_cart_order_session_exists($current_order->order_id, TRUE)) {
      return TRUE;
    }
    drupal_goto('<front>');
    return FALSE;
  }
  else {
    // Check access for authorized users.
    $current_order = $context->data;
    $order_user = $current_order->uid;
    if ($order_user != $user->uid) {
      drupal_goto('<front>');
      return FALSE;
    }
    return TRUE;
  }
}

/**
 * Provide a summary description based upon the checked roles.
 */
function order_owner_access_summary($conf, $context) {
  return t('Current user is owner of the order');
}
