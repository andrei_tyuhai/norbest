<?php
/**
 * @file
 *
 * Template for the CondeNast Home layout.
 */
?>

<section>
  <?php print $content['content']; ?>
</section>