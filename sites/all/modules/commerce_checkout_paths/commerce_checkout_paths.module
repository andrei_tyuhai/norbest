<?php
/**
 * @file
 * Drupal Commerce Custom Checkout pages paths module file.
 */

/**
 * Implements hook_menu().
 *
 * Provide a menu route for a configuration page.
 */
function commerce_checkout_paths_menu() {
  $items = array();
  $items['admin/commerce/config/commerce-checkout-paths'] = array(
    'title'             => 'Commerce checkout paths',
    'description'       => 'Commerce checkout paths settings',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('commerce_checkout_paths_settings_form'),
    'access arguments'  => array('administer site configuration'),
    'type'              => MENU_NORMAL_ITEM,
    'file'              => 'commerce_checkout_paths.admin.inc',
    'file path'         => drupal_get_path('module', 'commerce_checkout_paths'),
  );
  return $items;
}

/**
 * Implements hook_url_inbound_alter().
 *
 * Alter the system commerce checkout URLs.
 */
function commerce_checkout_paths_url_inbound_alter(&$path, $original_path, $path_language) {
  if (empty($path)) {
    $path = $original_path;
  }
  // Alter the system commerce checkout URLs.
  _commerce_checkout_paths_alter_path($path);
}

/**
 * Implements hook_url_outbound_alter().
 *
 * Alter the system commerce checkout URLs.
 */
function commerce_checkout_paths_url_outbound_alter(&$path, &$options, $original_path) {
  if (empty($path)) {
    $path = $original_path;
  }
  // Alter the system commerce checkout URLs.
  $parts = explode('/', $original_path);
  if ($parts[0] == 'checkout') {
    $order_id = !empty($parts[1]) ? $parts[1] : NULL;
    $page = !empty($parts[2]) ? $parts[2] : 'checkout';
    $custom_path = _commerce_checkout_paths_custom_checkout_path($page, $order_id);
    if ($custom_path) {
      $path = $custom_path;
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function commerce_checkout_paths_form_commerce_checkout_form_alter($form, &$form_state, $form_id) {
  if (variable_get('commerce_checkout_paths_auto_redirect', FALSE)) {
    if (strpos($form_id, 'checkout_') !== FALSE) {
      // Add an additional checkout form submit
      // handlers for "Continue" and "Back" buttons.
      $form['buttons']['continue']['#submit'][] = 'commerce_checkout_paths_continue_submit';
      $form['buttons']['back']['#submit'][]     = 'commerce_checkout_paths_back_submit';
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function commerce_checkout_paths_form_alter(&$form, &$form_state, $form_id) {
  if (strpos($form_id, 'commerce_cart') !== FALSE && variable_get('commerce_checkout_paths_auto_redirect', FALSE)) {
    $form['actions']['checkout']['#submit'][] = 'commerce_checkout_paths_cart_to_checkout';
  }
}

/**
 * Submit custom handler for the "Checkout" button of the cart form.
 */
function commerce_checkout_paths_cart_to_checkout($form, &$form_state) {
  if (isset($form_state['values']['op']) && $form_state['values']['op'] == t('Checkout')) {
    $checkout = strtolower($form_state['values']['op']);
    $form_state['redirect'] = _commerce_checkout_paths_get_custom_path($checkout);
  }
}

/**
 * Submit custom handler for the continue button of the checkout form.
 *
 * @param array $form
 *   Drupal form array.
 *
 * @param array &$form_state
 *   Drupal form state array.
 *
 * @ingroup forms
 * @see _commerce_checkout_paths_get_custom_path()
 */
function commerce_checkout_paths_continue_submit($form, &$form_state) {
  // If there is another checkout page...
  if ($form_state['checkout_page']['next_page']) {
    // Redirect to the next checkout page.
    $form_state['redirect'] = _commerce_checkout_paths_get_custom_path($form_state['checkout_page']['next_page']);
  }
}

/**
 * Special custom submit handler for the back button to avoid processing orders.
 *
 * @param array $form
 *   Drupal form array.
 *
 * @param array &$form_state
 *   Drupal form state array.
 *
 * @ingroup forms
 * @see _commerce_checkout_paths_get_custom_path()
 */
function commerce_checkout_paths_back_submit($form, &$form_state) {
  // If there is another checkout page...
  if ($form_state['checkout_page']['prev_page']) {
    // Redirect to the previous checkout page.
    $form_state['redirect'] = _commerce_checkout_paths_get_custom_path($form_state['checkout_page']['prev_page']);
  }
}

/**
 * Get custom path value from admin settings data.
 *
 * @param string $page_id
 *   Page ID.
 *
 * @return string
 *   Custom path.
 */
function _commerce_checkout_paths_get_custom_path($page_id) {
  $path = variable_get('commerce_checkout_paths_' . $page_id, FALSE);
  return $path !== FALSE ? $path : '';
}

/**
 * Altering commerce checkout paths.
 *
 * @param string &$path
 *   Path.
 */
function _commerce_checkout_paths_alter_path(&$path) {
  global $user;
  // Get an Order ID by User ID.
  $order_id = commerce_cart_order_id($user->uid);
  // If Order ID exist.
  if ($order_id) {
    foreach (_commerce_checkout_paths_get_checkout_pages_list() as $name => $value) {
      $custom_path = _commerce_checkout_paths_custom_checkout_path($name, $order_id);
      if ($path == $custom_path) {
        $temp_path = 'checkout/' . $order_id . '/' . $name;
        // Check if path is valid.
        if (drupal_valid_path($temp_path)) {
          $path = $temp_path;
          break;
        }
      }
    }
  }
}

/**
 * Get custom checkout path.
 *
 * @param string $page
 *   Checkout page key.
 *
 * @param int $order_id
 *   Order ID - defaults to current user's cart ID.
 *
 * @return string
 *   Custom checkout path.
 */
function _commerce_checkout_paths_custom_checkout_path($page = 'checkout', $order_id = NULL) {
  if (empty($order_id)) {
    global $user;
    // Get an Order ID by User ID.
    $order_id = commerce_cart_order_id($user->uid);
  }
  if ($order_id) {
    // Get custom path which has been set in admin config page.
    // Returned FALSE, if it doesn't set.
    $custom_path = variable_get('commerce_checkout_paths_' . $page, FALSE);
    // Check if custom path has been set.
    if ($custom_path !== FALSE) {
      // Replace tokens.
      $custom_path = token_replace($custom_path, array('commerce_order' => commerce_order_load($order_id)));
      $custom_path = preg_replace('/[^a-z0-9\/]+/', '-', strtolower(check_url($custom_path)));
      return $custom_path;
    }
  }
  return FALSE;
}

/**
 * Getting a checkout pages array.
 *
 * @return array
 *   Array of checkout pages data.
 */
function _commerce_checkout_paths_get_checkout_pages_list() {
  $checkout_pages = array();
  // Get commerce checkout pages list.
  foreach (commerce_checkout_pages() as $values) {
    // Create a data array with titles and descriptions,
    // indexed by checkout page machine_name.
    $checkout_pages[$values['page_id']]['title']       = $values['title'];
    $checkout_pages[$values['page_id']]['description'] = $values['help'];
  }
  return $checkout_pages;
}
