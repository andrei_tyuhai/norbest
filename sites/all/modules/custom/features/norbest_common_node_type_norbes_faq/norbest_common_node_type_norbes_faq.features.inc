<?php
/**
 * @file
 * norbest_common_node_type_norbes_faq.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function norbest_common_node_type_norbes_faq_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function norbest_common_node_type_norbes_faq_node_info() {
  $items = array(
    'norbest_faq' => array(
      'name' => t('Norbest FAQ'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
