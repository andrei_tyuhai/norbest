<?php
/**
 * @file
 * Move country field to the very end.
 */

$plugin = array(
  'title' => t('Move country field to the very end.'),
  'format callback' => 'meshbrains_addressfield_format_address_alter_country',
  'type' => 'address',
  'weight' => -80,
);

/**
 * Format callback.
 *
 * @see CALLBACK_addressfield_format_callback()
 */
function meshbrains_addressfield_format_address_alter_country(&$format, $address, $context = array()) {
  // Move country field to the very end.
  $format['country']['#weight'] = 100;
}

