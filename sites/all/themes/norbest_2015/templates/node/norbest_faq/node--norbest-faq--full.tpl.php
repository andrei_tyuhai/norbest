<?php

/**
 * @file.
 *
 *  CT FAQ, View mode Full.
 */
?>
<div class="faq-section <?php print $classes; ?>">
  <?php if (!empty($_html['questions'])): ?>
    <?php foreach ($_html['questions'] as $category => $items): ?>
      <div class="category">
        <span class="title"><?php print $category?></span>
        <?php if (!empty($items)): ?>
          <ul class="question-list">
            <?php foreach ($items as $key => $item): ?>
              <?php if (!empty($item['question'])): ?>
                <li class="opener">
                  <span class="question"><?php print $item['question']; ?></span>
                  <?php if (!empty($item['answer']) && !empty($item['answer']['value'])): ?>
                    <div class="answer">
                      <div class="answer-wrap"> <?php print $item['answer']['safe_value']; ?></div>
                    </div>
                  <?php endif; ?>
                </li>
              <?php endif; ?>
            <?php endforeach; ?>
          </ul>
        <?php endif ;?>
      </div>
    <?php endforeach; ?>
  <?php endif; ?>
</div>
