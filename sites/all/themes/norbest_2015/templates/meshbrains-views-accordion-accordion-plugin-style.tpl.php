<div id="views-bootstrap-accordion-<?php print $id ?>" class="<?php print $classes ?>">
  <?php reset($rows);
        $first = key($rows); ?>
  <?php foreach ($rows as $key => $row): ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        
        <h2 class="panel-title">
          <a class="accordion-toggle <?php if ($key !== $first): ?>collapsed<?php endif; ?>"
             data-toggle="collapse"
             data-parent="#views-bootstrap-accordion-<?php print $id ?>"
             href="#collapse<?php print $key ?>">
             <div class='icon-image hidden-xs'><?php print $view->render_field('field_icon', $key); ?></div>
             <div class='title'><?php print $titles[$key] ?></div>
          </a>
        </h2>
      </div>
      <div id="collapse<?php print $key ?>" class="panel-collapse collapse <?php if($key == '0'): ?>in<?php endif; ?>">
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-12 col-sm-11 col-sm-offset-1">
              <?php print $row ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endforeach ?>
</div>
