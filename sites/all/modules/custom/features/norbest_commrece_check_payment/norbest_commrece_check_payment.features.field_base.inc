<?php
/**
 * @file
 * norbest_commrece_check_payment.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function norbest_commrece_check_payment_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_compliments_of'.
  $field_bases['field_compliments_of'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_compliments_of',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  return $field_bases;
}
