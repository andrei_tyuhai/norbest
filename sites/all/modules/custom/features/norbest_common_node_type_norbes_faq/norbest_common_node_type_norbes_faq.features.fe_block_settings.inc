<?php
/**
 * @file
 * norbest_common_node_type_norbes_faq.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function norbest_common_node_type_norbes_faq_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-certificate_by_amount_header'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'certificate_by_amount_header',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'gift-certificates/by-amount',
    'roles' => array(),
    'themes' => array(
      'norbest_2015' => array(
        'region' => 'offset_header',
        'status' => 1,
        'theme' => 'norbest_2015',
        'weight' => -41,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['block-checkout_review_header'] = array(
    'cache' => -1,
    'css_class' => 'no-print',
    'custom' => 0,
    'machine_name' => 'checkout_review_header',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'checkout/*/review',
    'roles' => array(),
    'themes' => array(
      'norbest_2015' => array(
        'region' => 'offset_header',
        'status' => 1,
        'theme' => 'norbest_2015',
        'weight' => -37,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'Review Your Order',
    'visibility' => 1,
  );

  $export['block-checkout_shipping_header'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'checkout_shipping_header',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'checkout/*/shipping',
    'roles' => array(),
    'themes' => array(
      'norbest_2015' => array(
        'region' => 'offset_header',
        'status' => 1,
        'theme' => 'norbest_2015',
        'weight' => -38,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'Shipping Your Norbest Gift Certificates',
    'visibility' => 1,
  );

  $export['block-faq_header_block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'faq_header_block',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'faq',
    'roles' => array(),
    'themes' => array(
      'norbest_2015' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'norbest_2015',
        'weight' => -54,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['block-whole_turkey_certificate_header'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'whole_turkey_certificate_header',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'gift-certificates/by-weight',
    'roles' => array(),
    'themes' => array(
      'norbest_2015' => array(
        'region' => 'offset_header',
        'status' => 1,
        'theme' => 'norbest_2015',
        'weight' => -42,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
