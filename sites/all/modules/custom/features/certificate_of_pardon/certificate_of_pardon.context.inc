<?php
/**
 * @file
 * certificate_of_pardon.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function certificate_of_pardon_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'pardon_certificate';
  $context->description = 'Certificate of Pardon';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'certificate-pardon' => 'certificate-pardon',
      ),
    ),
  );
  $context->reactions = array(
    'template_suggestions' => 'page__node__webform__pardon',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Certificate of Pardon');
  $export['pardon_certificate'] = $context;

  return $export;
}
