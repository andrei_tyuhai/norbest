<?php
/**
 * @file
 * norbest_commrece_check_payment.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function norbest_commrece_check_payment_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
