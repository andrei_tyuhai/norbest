<?php if (isset($content['commerce_line_items'])) : ?>
  <div class="checkout_review form-wrapper form-group complete-order"
       id="edit-checkout-review">
    <div class="checkout_review form-wrapper form-group"
         id="edit-checkout-review">
      <table class="checkout-review table table-striped">
        <tbody>
        <tr class="pane-title odd odd">
          <td colspan="2">Shopping cart contents</td>
        </tr>
        <tr class="pane-data even even">
          <td colspan="2" class="pane-data-full">
            <?php print $content['commerce_line_items']; ?>
          </td>
        </tr>
        <tr class="pane-title odd odd">
          <td colspan="2">Account information</td>
        </tr>
        <tr class="pane-data even even">
          <td colspan="2" class="pane-data-full">
            <div class="form-type-item form-item form-group">
              <label>E-mail address </label>
              <?php print $content['mail'] ;?>
            </div>
          </td>
        </tr>
        <tr class="pane-title odd odd">
          <td colspan="2">Shipping information</td>
        </tr>
        <tr class="pane-data even even">
          <td colspan="2" class="pane-data-full">
            <?php print render($content['commerce_customer_billing']); ?>
          </td>
        </tr>
        <tr class="pane-title odd odd">
          <td colspan="2">Billing information</td>
        </tr>
        <tr class="pane-data even even">
          <td colspan="2" class="pane-data-full">
            <?php print render($content['commerce_customer_shipping']); ?>
          </td>
        </tr>
        </tbody>
      </table>
    </div>
  </div>
<?php endif ;?>
