<?php

/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>> 
  <div class="row">
    <div class="col-xs-12 col-md-8 col-top">
      <img src="<?php print $image['url']; ?>" width=100%>
      
      <div class="product-support">
        <div class="row">
          <div class="col-xs-12 product-description">
            <h4><?php print $title; ?></h4>
            <?php if (!empty($body['value'])): ?>
            <?php print $body['value'] ?>
            <?php endif; ?>  
          </div><!-- .col -->
        </div><!-- .row -->
        
        <div class='row'>
          <div class='social-icons'>
            <div class='col-xs-2 col-xs-offset-2'>              
              <a href="<?php url(drupal_get_path_alias('node/' . $node->nid), array('absolute' => TRUE)); ?>" data-image="<?php print $GLOBALS['base_url'] . $image['large_url']; ?>" data-desc="<?php print $title ?>" class="pinterest-color pinit center-block"></a>
            </div>
            <div class='col-xs-2'>
              <a href="https://www.facebook.com/sharer/sharer.php?u=<?php print url(drupal_get_path_alias('node/' . $node->nid), array('absolute' => TRUE)); ?>" target="_blank" class="facebook-color center-block"></a>
            </div>
            <div class='col-xs-2'>
              <a href="#" class="print-color center-block" onclick="window.print();return false;"></a>
            </div>
            <div class='col-xs-2'>
              <a href="mailto:someone@example.com?subject=Excited%20about%20Norbest&amp;body=Come%20visit%20<?php print url(drupal_get_path_alias('node/' . $node->nid), array('absolute' => TRUE)); ?>" target="_top" class='email-color center-block'></a>
            </div>
          </div><!-- .social-icons -->
        </div><!-- .row -->
      </div><!-- .recipe-support -->
      
      <div class="row">
        <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
          <section class="performance-facts">
            <header class="performance-facts__header">
              <h2 class="performance-facts__title">Nutrition Facts</h2>
              <p>Serving Size <?php print $serving_size ?>
              <p>Serving Per Container <?php print $servings_per_container ?></p>
            </header>
            <table class="performance-facts__table">
              <thead>
                <tr>
                  <th colspan="3" class="small-info">
                    Amount Per Serving
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th colspan="2">
                    <b>Calories</b>
                    <?php print $calories ?>
                  </th>
                  <td>
                    Calories from Fat
                    <?php print $calories_from_fat ?>
                  </td>
                </tr>
                <tr class="thick-row">
                  <td colspan="3" class="small-info">
                    <b>% Daily Value*</b>
                  </td>
                </tr>
                <tr>
                  <th colspan="2">
                    <b>Total Fat</b>
                    <?php print $total_fat ?>g
                  </th>
                  <td>
                    <b><?php print $total_fat_percent_dv ?>%</b>
                  </td>
                </tr>
                <tr>
                  <td class="blank-cell">
                  </td>
                  <th>
                    Saturated Fat
                    <?php print $saturated_fat ?>g
                  </th>
                  <td>
                    <b><?php print $saturated_fat_percent_dv ?>%</b>
                  </td>
                </tr>
                <tr>
                  <td class="blank-cell">
                  </td>
                  <th>
                    Trans Fat
                    <?php print $trans_fat ?>g
                  </th>
                  <td>
                  </td>
                </tr>
                <tr>
                  <th colspan="2">
                    <b>Cholesterol</b>
                    <?php print $cholesterol ?>mg
                  </th>
                  <td>
                    <b><?php print $cholesterol_percent_dv ?>%</b>
                  </td>
                </tr>
                <tr>
                  <th colspan="2">
                    <b>Sodium</b>
                    <?php print $sodium ?>mg
                  </th>
                  <td>
                    <b><?php print $sodium_percent_dv ?>%</b>
                  </td>
                </tr>
                <tr>
                  <th colspan="2">
                    <b>Total Carbohydrate</b>
                    <?php print $total_carbs ?>g
                  </th>
                  <td>
                    <b><?php print $total_carbs_percent_dv ?>%</b>
                  </td>
                </tr>
                <tr>
                  <td class="blank-cell">
                  </td>
                  <th>
                    Dietary Fiber
                    <?php print $dietary_fiber ?>g
                  </th>
                  <td>
                    <b><?php print $dietary_fiber_percent_dv ?>%</b>
                  </td>
                </tr>
                <tr>
                  <td class="blank-cell">
                  </td>
                  <th>
                    Sugars
                    <?php print $sugars ?>g
                  </th>
                  <td>
                  </td>
                </tr>
                <tr class="thick-end">
                  <th colspan="2">
                    <b>Protein</b>
                    <?php print $protein ?>g
                  </th>
                  <td>
                  </td>
                </tr>
              </tbody>
            </table>
            
            <table class="performance-facts__table--grid">
              <tbody>
                <tr>
                  <td colspan="2">
                    Vitamin A
                    <?php print $vitamin_a_percent_dv ?>%
                  </td>
                  <td>
                    Vitamin C
                    <?php print $vitamin_c_percent_dv ?>%
                  </td>
                </tr>
                <tr class="thin-end">
                  <td colspan="2">
                    Calcium
                    <?php print $calcium_percent_dv ?>%
                  </td>
                  <td>
                    Iron
                    <?php print $iron_percent_dv ?>%
                  </td>
                </tr>
              </tbody>
            </table>
            
            <p class="small-info">* Percent Daily Values are based on a 2,000 calorie diet.</p>
            
          </section>
        </div><!-- .col -->
      </div><!-- .row -->
    </div><!-- .col -->
      
    <div class="hidden-xs hidden-sm">
	    <div class="col-xs-12 col-sm-4 col-top">
	      <div class="shopping-list">
	        <p class="shopping-list-title">Shopping List</p>
	        <div class="notes">
	          <div></div>
	          <div></div>
	          <div></div>
	          <div></div>
	          <div></div>
	          <div></div>
	          <div></div>
	          <div></div>
	          <div></div>
	          <div></div>
	          <div></div>
	          <div></div>
	          <div></div>
	          <div></div>
	          <div></div>
	          <div></div>            
	        </div><!-- .notes -->
	      </div><!-- .shopping-list -->
	    </div><!-- .col -->
    </div><!-- .hidden-* -->
  </div><!-- .row -->
</div>
