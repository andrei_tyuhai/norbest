<?php
/**
 * @file
 * norbest_commrece_check_payment.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function norbest_commrece_check_payment_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'config_pages-global_site_settings-field_faq_page_title'.
  $field_instances['config_pages-global_site_settings-field_faq_page_title'] = array(
    'bundle' => 'global_site_settings',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_faq_page_title',
    'label' => 'FAQ page title',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'config_pages-global_site_settings-field_gift_certificates_text'.
  $field_instances['config_pages-global_site_settings-field_gift_certificates_text'] = array(
    'bundle' => 'global_site_settings',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'config_pages',
    'field_name' => 'field_gift_certificates_text',
    'label' => 'Gift Certificates Text',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('FAQ page title');
  t('Gift Certificates Text');

  return $field_instances;
}
