commerce_checkout_paths
=======================

Drupal Commerce Checkout paths.

This module allows users to create a custom paths
for Drupal commerce checkout pages.

Requirements:
 <b>Drupal Commerce.</b>

Installation:
 - install module
 - go to admin/commerce/config/commerce-checkout-paths page 
   and set a paths for checkout pages which you want
