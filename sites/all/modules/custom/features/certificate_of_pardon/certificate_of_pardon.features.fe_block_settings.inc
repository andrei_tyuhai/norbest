<?php
/**
 * @file
 * certificate_of_pardon.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function certificate_of_pardon_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-pardon_certificate_desc'] = array(
    'cache' => -1,
    'css_class' => 'pardon-certificate-desc',
    'custom' => 0,
    'machine_name' => 'pardon_certificate_desc',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'certificate-pardon
content/certificate-pardon',
    'roles' => array(),
    'themes' => array(
      'norbest_2015' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'norbest_2015',
        'weight' => -50,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['block-pardon_certificate_rules'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'pardon_certificate_rules',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'certificate-pardon
content/certificate-pardon',
    'roles' => array(),
    'themes' => array(
      'norbest_2015' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'norbest_2015',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
