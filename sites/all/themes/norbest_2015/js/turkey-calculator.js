(function($, view) {
  var Turkey = (function() {
    var onSizeChange = function() {
      var adults = $(view.num_adults).val();
      adults = Math.round(Math.max(0, ($.isNumeric(adults) ? adults : 0)));
      var children = $(view.num_children).val();
      children = Math.round(Math.max(0, ($.isNumeric(children) ? children : 0)));
      var leftovers = ($(view.want_leftovers).is(":checked") ? 1.5 : 1);
      var pounds = (adults + (children / 2)) * leftovers;
      pounds = validateWeightLbs(pounds);
      var metric = lbs2kg(pounds);
      var text = roundWeight( pounds ) + " lbs or " + roundWeight( metric ) + " kg";
      $(view.num_adults).val( adults );
      $(view.num_children).val( children );
      $(view.num_results).text( text );
      $(view.thaw_lbs).val(pounds);
      $(view.thaw_kgs).val(metric);
      $(view.cook_lbs).val(pounds);
      $(view.cook_kgs).val(metric);
      onThawTimeChange(false);
      onCookTimeChange(false);
    },
    onThawTimeChange = function(isMetric) {
      var pounds = $(view.thaw_lbs).val();
      var metric = $(view.thaw_kgs).val();
      if(isMetric) {
        metric = Math.max(0, ($.isNumeric(metric) ? metric : 0));
        pounds = kg2lbs(metric);
      } else {
        pounds = Math.max(0, ($.isNumeric(pounds) ? pounds : 0));
      }
      pounds = roundWeight(validateWeightLbs(pounds));
      metric = roundWeight(lbs2kg(pounds));
      var time_water = toTimeString(pounds);
      var time_frige = toTimeString(pounds * 5);
      if(isMetric) {
        $(view.thaw_lbs).val(pounds);
      } else {
        $(view.thaw_kgs).val(metric);
      }
      $(view.thaw_fridge_results).text(time_frige);
      $(view.thaw_water_results).text(time_water);
    },
    onCookTimeChange = function(isMetric) {
      var pounds = $(view.cook_lbs).val();
      var metric = $(view.cook_kgs).val();
      if(isMetric) {
        metric = Math.max(0, ($.isNumeric(metric) ? metric : 0));
        pounds = kg2lbs(metric);
      } else {
        pounds = Math.max(0, ($.isNumeric(pounds) ? pounds : 0));
      }
      pounds = roundWeight(validateWeightLbs(pounds));
      metric = roundWeight(lbs2kg(pounds));
      var txt_stuffed = "";
      var txt_unstuffed = "";
      $.each(cookingChart, function(idx, val) {
        if((pounds >= val.min) && (pounds < val.max)) {
          txt_stuffed = val.stuffed;
          txt_unstuffed = val.unstuffed;
          return false;
        }
      });
      if(isMetric) {
        $(view.cook_lbs).val(pounds);
      } else {
        $(view.cook_kgs).val(metric);
      }
      $(view.cook_unstuffed_results).html(txt_unstuffed);
      $(view.cook_stuffed_results).html(txt_stuffed);
    },
    lbs2kg = function(lbs) { return lbs / 2.20462; },
    kg2lbs = function(kg) { return kg * 2.20462; },
    roundWeight = function(weight) {
      return Math.round( weight * 10 ) / 10
    },
    validateWeightLbs = function(pounds) {
      if (pounds > 40) {
        pounds = 0;
      }
      return Math.min(40, Math.max(0, pounds));
    },
    toTimeString = function(hours) {
      var days = Math.floor(hours / 24);
      hours = Math.floor(hours % 24);
      var txt = days <= 0 ? "" : ( days == 1 ? "1 day " : days + " days ");
      txt += hours <= 0 ? "0 hours" : ( hours == 1 ? "1 hour" : hours + " hours");
      return txt;
    },
    cookingChart = [
        { "min" : 0, "max" : 4, "unstuffed" : "0 hours", "stuffed" : "0 hours"},
        { "min" : 4, "max" : 8, "unstuffed" : "2 &frac12; to 2 &frac34; hours", "stuffed" : "3 to 3 &frac14; hours"},
        { "min" : 8, "max" : 12, "unstuffed" : "2 &frac34; to 3 hours", "stuffed" : "3  &frac12; to 3 &frac34; hours"},
        { "min" : 12, "max" : 14, "unstuffed" : "3 to 3 &frac34; hours", "stuffed" : "3 &frac34; to 4 hours"},
        { "min" : 14, "max" : 18, "unstuffed" : "3 &frac34; to 4 &frac14; hours", "stuffed" : "4  &frac14; to 4 &frac12; hours"},
        { "min" : 18, "max" : 20, "unstuffed" : "4 &frac14; to 4 &frac12; hours", "stuffed" : "4  &frac12; to 4 &frac34; hours"},
        { "min" : 20, "max" : 24, "unstuffed" : "4 &frac12; to 5 hours", "stuffed" : "4 &frac34; to 5 &frac12; hours"},
        { "min" : 24, "max" : 28, "unstuffed" : "5 to 5 &frac14; hours", "stuffed" : "5 &frac12; to 6 &frac14; hours"},
        { "min" : 28, "max" : 32, "unstuffed" : "5 &frac14; to 5 &frac12; hours", "stuffed" : "6 &frac14; to 6 &frac34; hours"},
        { "min" : 32, "max" : 36, "unstuffed" : "5 &frac12; to 5 &frac34; hours", "stuffed" : "6 &frac34; to 7 &frac14; hours"},
        { "min" : 36, "max" : 40, "unstuffed" : "5 &frac34; to 6 hours", "stuffed" : "7 &frac14; to 7 &frac34; hours"},
        { "min" : 40, "max" : 41, "unstuffed" : "6 hours", "stuffed" : "7 &frac34; hours"},
        { "min" : 41, "max" : 99, "unstuffed" : "0 to 0 hours", "stuffed" : "0 to 0 hours"}
      ],
      defaults = {
      "num_adults" : "",
      "num_children" : "",
      "want_leftovers" : "",
      "thaw_lbs" : "",
      "thaw_kgs" : "",
      "cook_lbs" : "",
      "cook_kgs" : "#",
      "num_results" : "",
      "thaw_fridge_results" : "",
      "thaw_water_results" : "",
      "cook_unstuffed_results" : "",
      "cook_stuffed_results" : ""
      };
    return {
      Init: function() {
        $.extend({}, defaults, view);
        $(view.num_adults + " , " + view.num_children).on("change keyup",onSizeChange);
        $(view.want_leftovers).on("change click",onSizeChange);
        $(view.thaw_lbs).on("change keyup",function(){onThawTimeChange(false);});
        $(view.thaw_kgs).on("change keyup",function(){onThawTimeChange(true);});
        $(view.cook_lbs).on("change keyup",function(){onCookTimeChange(false);});
        $(view.cook_kgs).on("change keyup",function(){onCookTimeChange(true);});
      }
    };
  })();
  $(function() { Turkey.Init(); });
})(jQuery,
    {
      "num_adults" : "#num-adults", "num_children" : "#num-children", "want_leftovers" : "#want-leftovers",
      "thaw_lbs" : "#thaw-lbs", "thaw_kgs" : "#thaw-kgs",
      "cook_lbs" : "#cook-lbs", "cook_kgs" : "#cook-kgs",
      "num_results" : "#size-results",
      "thaw_fridge_results" : "#thaw-fridge-results", "thaw_water_results" : "#thaw-water-results",
      "cook_unstuffed_results" : "#cook-unstuffed-results", "cook_stuffed_results" : "#cook-stuffed-results"
    });