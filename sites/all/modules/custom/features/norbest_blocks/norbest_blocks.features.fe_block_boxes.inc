<?php
/**
 * @file
 * norbest_blocks.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function norbest_blocks_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Turkey Ham Veggie Static Offset Header';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'certificate_by_amount_header';
  $fe_block_boxes->body = '<h1>Turkey, Ham or Veggie Gift Certificates<br />
by $ Amount</h1>

<p>A turkey, ham or veggie certificate entitles the holder to any of the three items up to a specific dollar amount. A safe route if you\'re unsure about the dietary restrictions of the recipient(s).</p>
';

  $export['certificate_by_amount_header'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Review Order Static Offset Block';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'checkout_review_header';
  $fe_block_boxes->body = '<p>Ensure that your order, billing, and shipping information is correct. You may pay by check or credit card.</p>
';

  $export['checkout_review_header'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Shipping Static Offset Header';
  $fe_block_boxes->format = 'filtered_html';
  $fe_block_boxes->machine_name = 'checkout_shipping_header';
  $fe_block_boxes->body = '<h1>We provide two options for shipping. Please check the shipping option that serves you the best.</h1>
';

  $export['checkout_shipping_header'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Norbest Gift Certificates Faq Header Block';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'faq_header_block';
  $fe_block_boxes->body = '<div class="center">
<h2 class="rtecenter">Learn more about Norbest turkey gift certificates<br />
by browsing our frequency asked questions</h2>

<div class="faq-help-text">
<p class="rtecenter">Still have questions? Feel free to give us a call at<br />
800-418-9522 or email us at&nbsp;<a href="mailto:giftcertificates@norbest.com">giftcertificates@norbest.com</a></p>
</div>
</div>
';

  $export['faq_header_block'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Whole Turkey Static Offset Header';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'whole_turkey_certificate_header';
  $fe_block_boxes->body = '<h1>Whole Turkey Gift Certificates by Weight</h1>

<p>Norbest turkey by weight certificate entitles the holder to a whole turkey up to the weight<br />
indicated. This type of certificate is simple and sure to please.</p>
';

  $export['whole_turkey_certificate_header'] = $fe_block_boxes;

  return $export;
}
